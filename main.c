#include <sndfile.h>
#include "feature.h"
#include <mpi.h>
double example_feature_mat[dim1][dim2];
double sample_feature_mat[dim1][dim2];
double example_feature_mat1[dim1][dim2];
double sample_feature_mat1[dim1][dim2];
double results_train[dim1];
double results_test[dim1];
char *infilename1 = NULL;
char *infilename2 = NULL;
SNDFILE     *infile1 = NULL;
SNDFILE     *infile2 = NULL;
SF_INFO     sfinfo;
double win = 0.0;
double step = 0.0;
int winsize = 0;
int stepsize = 0;
int frameidx = 0;
int readcount = 0;
short *clip1 = NULL;
short *clip2 = NULL;
int idx = 0;
char *f1;
char *f2;
pthread_t threads[MAX_THREAD];
int thread_id[MAX_THREAD];
int sz;
int calc1 = 0;
int calc2 = 0;

long get_file_size( char *filename )
{
  FILE *fp;
  long n;

  /* Open file */
  fp = fopen(filename, "rb");

  /* Ignoring error checks */

  /* Find end of file */
  fseek(fp, 0L, SEEK_END);

  /* Get current position */
  n = ftell(fp);

  /* Close file */
  fclose(fp);

  /* Return the file size*/
  return n;
}

void* func(void* arg) 
{ 
    int thread_part = *(int*)arg;
    int start = thread_part*sz/MAX_THREAD;
    int end = (thread_part+1)*sz/MAX_THREAD;
    for (frameidx = start; frameidx < end; frameidx++)
    {
        sf_seek(infile1, frameidx * stepsize, SEEK_SET);
	readcount = sf_readf_short(infile1, clip1, winsize);
        if(readcount != winsize)
            {
            break;
            }

	feature_extraction_main(clip1, frameidx, example_feature_mat);
	++frameidx;
    }

    MPI_Scatter(&sample_feature_mat, dim1*dim2, MPI_DOUBLE, sample_feature_mat1, dim1*dim2, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Scatter(&example_feature_mat, dim1*dim2, MPI_DOUBLE, example_feature_mat1, dim1*dim2, MPI_DOUBLE, 0, MPI_COMM_WORLD);
 

    calc1++;
    
    if (calc1 == 4) {
    	calc1 = 5;
    	twodarray2csv(example_feature_mat, f1); 
    	mlModel(f1, results_train);
    }
    
    for (frameidx = start; frameidx < end; frameidx++) 
        {
        sf_seek(infile2, frameidx * stepsize, SEEK_SET);
	readcount = sf_readf_short(infile2, clip2, winsize);
        if(readcount != winsize)
            {
            break;
            }

	feature_extraction_main(clip2, frameidx, example_feature_mat);
	++frameidx;
    }

    MPI_Scatter(&sample_feature_mat, dim1*dim2, MPI_DOUBLE, sample_feature_mat1, dim1*dim2, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Scatter(&example_feature_mat, dim1*dim2, MPI_DOUBLE, example_feature_mat1, dim1*dim2, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    calc2++;
    
    if (calc2 == 4) {
    	calc2 = 5;
    	twodarray2csv(example_feature_mat, f2);
    	mlModel(f2, results_test); 
    }
    
    MPI_Allgather(&example_feature_mat1, dim1*dim2, MPI_DOUBLE, example_feature_mat, dim1*dim2, MPI_DOUBLE, MPI_COMM_WORLD);
    MPI_Allgather(&sample_feature_mat1, dim1*dim2, MPI_DOUBLE, sample_feature_mat, dim1*dim2, MPI_DOUBLE, MPI_COMM_WORLD);

    
    return NULL;
}
int main (int argc, char *argv[])
{
    f1 = strdup("data_train.csv");
    f2 = strdup("data_test.csv"); 
    memset(&sfinfo, 0, sizeof(sfinfo));
    infilename1 = argv[1];
    infilename2 = argv[2];
    infile1 = sf_open(infilename1, SFM_READ, &sfinfo);
    infile2 = sf_open(infilename2, SFM_READ, &sfinfo);
    win = atof(argv[3]);
    step = atof(argv[4]);
    winsize = (int)(win * sfinfo.samplerate);
    stepsize = (int)(step * sfinfo.samplerate);
    clip1 = (short *)malloc(winsize * sizeof(short));
    clip2 = (short *)malloc(winsize * sizeof(short));
    feature_extraction_init(sfinfo.samplerate, winsize);
    sz = get_file_size(argv[2])/winsize; 
    int   numtasks, taskid, len;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
    MPI_Comm_rank(MPI_COMM_WORLD,&taskid);
    // Creating 4 threads 
    for (int i = 0; i < MAX_THREAD; i++) 
        pthread_create(&threads[i], NULL, func, &(thread_id[i])); 
  
    // joining 4 threads i.e. waiting for all 4 threads to complete 
    for (int i = 0; i < MAX_THREAD; i++) 
        pthread_join(threads[i], NULL); 

    feature_extraction_release();
    verify_template(results_train, results_test);
    sf_close(infile1);
    free(clip1);
    free(clip2);
    sf_close(infile2);

    MPI_Finalize();
    return 0 ;
}

