all: sound-features
sound-features: feature.c feature.h main.c
	mpic++ -g -o sound-features feature.c main.c -lm -lsndfile -std=c++11 -larmadillo -lmlpack -lboost_serialization -fopenmp 
clean:
	rm -rf sound-features
