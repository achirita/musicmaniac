# Music Maniac

Membrii:
	Alexandra Chirita
	Theodor Stoica

# Introducere

Problema analizei fisierelor audio reprezinta un subiect de interes in dezvoltarea de cod, data fiind utilitatea acestor aplicatii: medicina, social-media, armata etc. O directie in acest sens a fost definita de dorinta dezvoltatorilor de a putea analiza secvente de muzica interpretata, astfel incat sa se poata extrage diverse caracteristici ale melodiilor. O directie educationala a fost luata in acest sens prin aplicatii, precum Tonara sau Yousician.

# Motivatie

Aplicatiile de pana acum au un set de functionalitati limitate in ceea ce priveste analiza interpretarii muzicale. Tonara, de exemplu, ofera doar  detalii legate de acuratete, de tinerea masurii si de volum. Pentru a putea realiza un mediu de invatare prolific, o astfel de aplicatie ar trebui sa ia mai multe lucruri in considerare.

# Idee principala

Music Maniac este inceputul unei initiative in educatia muzicala. Aplicatia ar trebui pana la finalul perioadei sa indeplineasca urmatoarele puncte:
	1. Sa extraga feature-urile fisierelor audio de train si test, folosind atat resurse implementate de echipa, cat si open-source (https://github.com/lincolnhard/sound-feature-extraction-C sau/si Esstetica)
	2. Sa antreneze un model pentru identificarea mai multor caracteristici ale interpretarii: asemanare cu versiunea originala a cantecului, acuratete, continuitate, timpi etc.

# Ce s-a implementat pana acum

# Etapa 1
	
Asupra fisierelor audio au fost aplicate operatiile de normalizare si analiza spectrala. Apoi se aplica operatiile de mean si max pentru extragerea caracteristicilor fiecarui esantion. Fisierul audio este parcurs in ferestre, dupa o dimensiune data in argumentul functiei.

Dupa ce s-au extras aceste feature-uri, se creeaza un dataset care va fi folosit in model. Pana acum am facut la partea de ML doar analiza lineara (si am testat cateva exemple de acest fel din bibliotecile C++) pentru a extrage vectorul de distante din fisierul audio (pentru a afla distributia valorilor din matricea de feature-uri). Dupa am verificat daca aceasta distributie este asemenea in fisierul de train si cel de test. Aceasta parte ar putea fi folosita pentru a determina daca piesa interpretata e asemanatoare cu cea originala.

Timpi de rulare:
Test 1: 4.040s
Test 2: 4.266s
Test 3: 4.329s -> timpii de rulare s-au dublat aproape dupa ce am mai modificat unele prelucrari

# Etapa 2

In aceasta etapa am realizat profilinful aplicatiei seriale, rezultatele au fost ca trebuie paralelizare zonele de extragere de feature-uri si
de verificare, intrucat mlpack prelucreaza in paralel deja datele.

# Etapa 3

Aici am paralelizat cu openmp unele parti prelucrarea de feature-uri si verificarea.

# Etapa 4

Paralelizarea folosing pthreads a fost folosita in prelucrarile care folosesc ML.

# Etapa 5

Distribuirea feature-urilor intre thread-uri a fost facuta cu MPI.

# Ce folosim
Pe langa codul open-source mentionat, folosim si biblioteca mlpack din C++. Proiectul a fost compilat cu g++ deoarece C++ ofera mai multe biblioteci pentru ML decat C si metode mai rapide de procesare.
	
