#ifndef FFT_H
#define FFT_H

#include <stdio.h> // for printf
#include <stdlib.h> // for posix_memalign
#include <string.h> // for memset
#include <math.h> // for sqrt
#include <limits.h> //for SHRT_MAX
#include <time.h> // for clock_gettime
#include <wchar.h>
#include <fstream>
#include <iostream>
#define FEATURE_LEN 34
#define dim1 40000
#define dim2 34
#define MAX_THREAD 4
#include <mlpack/core.hpp>
#include <mlpack/methods/neighbor_search/neighbor_search.hpp>
#include <omp.h>
#include <pthread.h>
using namespace std;
using namespace mlpack;
using namespace mlpack::neighbor;
using namespace mlpack::metric;


typedef struct
    {
    double re;
    double im;
    }complex;

typedef struct
    {
    int fs;                             // sampling rate
    int signalsize;                     // input signal length (long-term)
    double **filterbanks;               // the triangular filterbank for MFCC computation
    int *chroma_nummat;                 // the chroma matrices (first half: nChroma, second half: nFreqsPerChroma[nChroma])
    double *features;                   // features (output)
    double *signal_prev_freq_domain;
    double signal_prev_freq_domain_sum;
    }feature_extraction_struct;
void feature_extraction_release
    (
    void
    );

double* feature_extraction_main
    (
    short *signal,
    int frame_idx,
    double v[dim1][dim2]
    );

void feature_extraction_init
    (
    const int FS,
    const int SIGLEN
    );
void mlModel(char *filename, double v[dim1]);
void twodarray2csv(double array[dim1][dim2], char* filename);
void verify_template(double v1[dim1], double v2[dim2]);
#endif // FFT_H
