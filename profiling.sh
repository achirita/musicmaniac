echo "Stat"
perf stat -e cpu-clock ./sound-features train.wav test.wav 0.00005 0.00005
perf stat -e cpu-clock ./sound-features Recording.wav Recording__2_.wav 0.00005 0.00005
perf stat -e cpu-clock ./sound-features Recording-3.wav Recording-5.wav 0.00005 0.00005
echo "Record"
perf record ./sound-features train.wav test.wav 0.00005 0.00005
perf record ./sound-features Recording.wav Recording__2_.wav 0.00005 0.00005
perf record ./sound-features Recording-3.wav Recording-5.wav 0.00005 0.00005


echo "Cycles"
perf stat -e cycles ./sound-features train.wav test.wav 0.00005 0.00005
perf stat -e cycles ./sound-features Recording.wav Recording__2_.wav 0.00005 0.00005
perf stat -e cycles ./sound-features Recording-3.wav Recording-5.wav 0.00005 0.00005

echo "Branch misses"
perf record -e instructions -c 1000000 ./sound-features train.wav test.wav 0.00005 0.00005
perf record -e instructions -c 1000000 ./sound-features Recording.wav Recording__2_.wav 0.00005 0.00005
perf record -e instructions -c 1000000 ./sound-features Recording-3.wav Recording-5.wav 0.00005 0.00005


